unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, LCLIntf;

type

  { TFUeber }

  TFUeber = class(TForm)
    Button1: TButton;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Label3MouseEnter(Sender: TObject);
    procedure Label3MouseLeave(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FUeber: TFUeber;

implementation

{$R *.lfm}

{ TFUeber }

// Programm-Info / Splash anzeigen

procedure TFUeber.Label3MouseEnter(Sender: TObject);
begin
  Label3.Font.Color:=clRed;
end;

procedure TFUeber.Label3Click(Sender: TObject);
begin
  OpenURL('https://github.com/PepeCyB/keulkulator');
end;

procedure TFUeber.Button1Click(Sender: TObject);
begin
  FUeber.Close;
end;

procedure TFUeber.Label3MouseLeave(Sender: TObject);
begin
  Label3.Font.Color:=clBlue;
end;

end.

