## Erstellen

Um die aktuelle Version zu erstellen, mittels

`git clone https://github.com/PepeCyB/keulkulator.git`

klonen oder als ZIP herunterladen und entpacken. Im Programmverzeichnis "keulkulator" befindet sich die Lazarus-Projektdatei "keulkulator.lpi".

Die Lazarus-IDE starten, das Projekt laden und mit Menü **Start** → **Kompilieren** (Strg-F9) oder in der Windows Eingabeaufforderung in das Programmverseichnis wechseln und mit

`lazbuild.exe keulkulator.lpi`

erstellen.

#### Abhängigkeiten

Zum Compilieren von Keulkulator ab Version 1.1.x wird das Komponenten-Paket [Eye-Candy Controls](http://wiki.freepascal.org/Eye-Candy_Controls) benötigt ([Download](https://packages.lazarus-ide.org/EyeCandyControls.zip)).