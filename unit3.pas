unit Unit3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Spin, Buttons, math, UData, LCLType;

type

  { TMatEditor }

  TMatEditor = class(TForm)
    BitBtAdd: TBitBtn;
    BitBtClear: TBitBtn;
    BitBtClose: TBitBtn;
    BitBtEntryOK: TBitBtn;
    BitBtOK: TBitBtn;
    BitBtSave: TBitBtn;
    EdMatName: TEdit;
    FlSpspzW: TFloatSpinEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LiBMaterial: TListBox;
    procedure BitBtAddClick(Sender: TObject);
    procedure BitBtClearClick(Sender: TObject);
    procedure BitBtCloseClick(Sender: TObject);
    procedure BitBtEntryOKClick(Sender: TObject);
    procedure BitBtOKClick(Sender: TObject);
    procedure BitBtSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LiBMaterialSelectionChange(Sender: TObject; User: boolean);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  MatEditor: TMatEditor;

implementation

{$R *.lfm}

{ TMatEditor }

procedure TMatEditor.FormCreate(Sender: TObject);
var
  i : Integer;
begin
  LiBMaterial.Items.Clear;
  for i := 0 to nummat - 1 do
    LiBMaterial.Items.Add(materialien[i] + ' -> ' + FloatToStrF(spezwid[i],ffFixed,2,2));
  LiBMaterial.ItemIndex := 0;
  EdMatName.Text := materialien[LiBMaterial.ItemIndex];
  FlSpspzW.Value := spezwid[LiBMaterial.ItemIndex];
end;

procedure TMatEditor.BitBtCloseClick(Sender: TObject);
var
  i : Integer;
begin
  configdat.Clear;
  configdat.LoadFromFile('material.dat');

  nummat := trunc(configdat.Count / 2);			// Double Count -> Int64

  materialien := TStringList.Create;                    // Materialien-Liste initialisieren
  SetLength(spezwid, nummat);                           // Array initialisieren

  for i := 0 to nummat-1 do
  begin
    materialien.Add(configdat[i*2]);			// Durchlauf in 2er-Schritten
    spezwid[i] := MyStrToFloat(configdat[i*2+1]);
  end;
  Close;
end;

procedure TMatEditor.BitBtEntryOKClick(Sender: TObject);
var
  i : Integer;
begin
  i := LiBMaterial.ItemIndex;
  materialien[i] := EdMatName.Text;
  spezwid[i] := FlSpspzW.Value;
  LiBMaterial.Items[i] := materialien[i] + ' -> ' + FloatToStrF(spezwid[i],ffFixed,2,2);
end;

procedure TMatEditor.BitBtOKClick(Sender: TObject);
begin
  Close;
end;

procedure TMatEditor.BitBtSaveClick(Sender: TObject);
var
  savelist : TStringList;
  i : Integer;
begin
  savelist := TStringList.Create;
  for i := 0 to nummat-1 do
  begin
    savelist.Add(materialien[i]);
    savelist.Add(FloatToStrF(spezwid[i],ffFixed,2,2));
  end;
  savelist.SaveToFile('material.dat');
  Application.MessageBox('Materialkonfiguration gespeichert!', 'Speichern',MB_ICONINFORMATION);
  close;
end;

procedure TMatEditor.BitBtClearClick(Sender: TObject);
begin
  EdMatName.Text := '';
  FlSpspzW.Value := 0.01;
end;

procedure TMatEditor.BitBtAddClick(Sender: TObject);
var
  i : Integer;
begin
  materialien.Add(EdMatName.Text);
  nummat := nummat + 1;
  SetLength(spezwid, nummat);
  spezwid[nummat - 1] := FlSpspzW.Value;
  LiBMaterial.Items.Clear;
  for i := 0 to nummat - 1 do
    LiBMaterial.Items.Add(materialien[i] + ' -> ' + FloatToStrF(spezwid[i],ffFixed,2,2));
  LiBMaterial.ItemIndex := 0;
  EdMatName.Text := materialien[LiBMaterial.ItemIndex];
  FlSpspzW.Value := spezwid[LiBMaterial.ItemIndex];
end;

procedure TMatEditor.FormShow(Sender: TObject);
var
  i : Integer;
begin
  LiBMaterial.Items.Clear;
  for i := 0 to nummat - 1 do
    LiBMaterial.Items.Add(materialien[i] + ' -> ' + FloatToStrF(spezwid[i],ffFixed,2,2));
  LiBMaterial.ItemIndex := 0;
  EdMatName.Text := materialien[LiBMaterial.ItemIndex];
  FlSpspzW.Value := spezwid[LiBMaterial.ItemIndex];
end;

procedure TMatEditor.LiBMaterialSelectionChange(Sender: TObject; User: boolean);
begin
  EdMatName.Text := materialien[LiBMaterial.ItemIndex];
  FlSpspzW.Value := spezwid[LiBMaterial.ItemIndex];
end;

end.

