# Keulkulator



**Ein Programm zur Berechnung von Coils in Selbstwickelverdampfern.**

Es können Materialien (Stand 21.06.2017: Kanthal A1, Kanthal A, Kanthal D, NiChrome N80, Nickel 200, V4A, V2A und Titan), Drahtstärke (mm und AWG), gewünschter Widerstand der Wicklung, Innendurchmesser der Wicklung und Befestigungsweg (Abstand Wicklung zu den Anschlussposts, WICHTIG: nur einfachen Weg eingeben), sowie die Anzahl der Coils (single, dual, quad) ausgewählt werden. Die Berechnung erfolgt sofort bei jeder Änderung eines Parameters.

Als Ergebnis wird die genaue erforderliche Drahtlänge, sowie die genaue Wicklungsanzahl ausgegeben. Da nur mit halben (die Drahtenden zeigen in zwei verschiedene Richtungen) und ganzen (die Drahtenden zeigen in die selbe Richtung) gearbeitet werden kann, werden drei verschiedene gerundete Ergebnisvarianten mit dem jeweils erreichten Widerstand angezeigt.

Keulkulator bietet nun auch die Möglichkeit, Flachdraht-Wicklungen zu berechnen. Zum Umschalten zwischen Rund- und Flachdraht wurde ein Schalter unter dem Material-Auswahlfeld eingebaut.

Bei Flachdraht kann die Breite und die Stärke des Ribbons ausgewählt werden.

Es kann bei Interesse die vom Akkuträger bereitgestellte Akkuspannung gewählt werden. Es werden dann Stromstärke und Leistung berechnet.

Die Setups können geladen und gespeichert werden. In der neuen Version werden auch die Daten für Flachdraht-Wicklungen gespeichert. Alte Dateiversionen (ohne Flachdraht-Daten) können weiterhin geladen werden. Eine Abwärts-Kompatibilität ist jedoch nicht gegeben. Dateien, die mit der neuen Version gespeichert wurden können mit Programmversionen ≦ 1.0.0 nicht geladen werden.



Das Programm wurde mit [Lazarus](http://www.lazarus-ide.org/)/[Freepascal](https://www.freepascal.org/) erstellt. Die vorhandene ausführbare Datei liegt unter einer Linux-Version vor. Sie wurde unter [Manjaro Linux](https://manjaro.org/) 17.0.1 Gellivara x86_64 erstellt.

Eine Windows-Version steht ebenfalls zur Verfügung (Windows 64 u. Windows 32).





![screenshot](screenshot.png)



Mit dem Material-Editor (Button neben dem Material-Auswahlfeld) können Parameter der vorliegenden Daten angepasst werden und auch neue Materialien mit dem jeweiligen spezifischen Widerstand zur Liste hinzugefügt werden.

![Material-Editor](screenshot_mat-ed.png)

Die Parameter eines in der Liste ausgewählten Materials können geändert werden. Die Änderungen werden mit dem Button "Übernehmen" übernommen.

Der Button "Felder löschen" löscht die Materialbezeichnung und setzt den spezifischen Widerstand zurück (0.01 Ohm/m). Es können neue Materialien eingegeben werden. Mit dem Button "Eintrag zufügen" wird das neue Material zur Liste hinzugefügt.

Der Button "OK" übernimmt die durchgeführten Änderungen und es wird ins Hauptprogramm zurückgekehrt. **Achtung:** Wird das Programm beendet, verfallen die Änderungen, sofern nicht vor Verlassen des Programms im Material-Editor mit dem Button "Speichern" die aktuelle Materialliste in der Konfigurationsdatei gespeichert wurde. Der Button "Speichern" speichert die aktuelle Liste und springt ins Hauptprogramm zurück.

Ein Klick auf den Button "Verwerfen" setzt die Materialliste auf den Stand vor den Änderungen zurück, sofern die geänderte Materialliste noch nicht gespeichert wurde.



## Dokumentation

Die Dokumentation befindet sich im Unterverzeichnis docs. Die Hilfe entweder direkt aus dem Programm aufrufen oder die Datei help.html bzs. help.md öffnen.



## Installation

Die aktuelle Version liegt als direkt ausfühbare Datei vor. Das Programmverzeichnis einfach ins gewünschte Zielverzeichnis kopieren und Keulkulator ausführen.

Das Programm steht auch als gepacktes Programm-Release (Windows-Version .ZIP - Linux-Version .tar.bz2) zur Verfügung.



## Erstellen

Um die aktuelle Version zu erstellen, mittels

`git clone https://github.com/PepeCyB/keulkulator.git`

klonen oder als ZIP herunterladen und entpacken. Im Programmverzeichnis "keulkulator" befindet sich die Lazarus-Projektdatei "keulkulator.lpi". 

Die Lazarus-IDE starten, das Projekt laden und mit Menü **Start** → **Kompilieren** (Strg-F9) oder im Terminal / Windows Eingabeaufforderung in das Programmverseichnis wechseln und mit

`lazbuild keulkulator.lpi`		(Linux)

bzw.

`lazbuild.exe keulkulator.lpi`	(Windows)

erstellen.

#### Abhängigkeiten

Zum Compilieren von Keulkulator ab Version 1.1.x wird das Komponenten-Paket [Eye-Candy Controls](http://wiki.freepascal.org/Eye-Candy_Controls) benötigt ([Download](https://packages.lazarus-ide.org/EyeCandyControls.zip)).



## Lizenz

Keulkulator ist lizensiert unter der [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl.txt).

[GNU GPL 3](gpl.txt)



Copyright 2017 PepeCyB

