program keulkulator;

{$mode objfpc}{$H+}

{
This file is part of Keulkulator.

Keulkulator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Keulkulator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Keulkulator.  If not, see <http://www.gnu.org/licenses/>.
}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, UData, Unit2, Unit3
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='Keulkulator';
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFUeber, FUeber);
  Application.CreateForm(TMatEditor, MatEditor);
  Application.Run;
end.

